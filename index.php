<?php 
	require_once "librerias/tcpdf/tcpdf.php";
    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->setCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("comprobante");
    $obj_pdf-> SetHeaderData('','',PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(false);
    $obj_pdf->setPrintFooter(false);
    $obj_pdf->SetAutoPageBreak(TRUE, 2);
    $obj_pdf->SetFont('helvetica', '', 12);
    $obj_pdf->AddPage('P', 'LETTER');
    $obj_pdf->Image('img/acalog.png', 20, 15, 30, 30, 'PNG', '', '', false, 300, '', false, false, 0, false, false, false);
     $obj_pdf->Image('img/380_medium.png', 20, 68, 40, 23, 'PNG', '', '', false, 300, '', false, false, 0, false, false, false);
    $content=<<<EOF
    <style>
        .subtitle
        {
            color: blue;
            font-size:10px;
        }
        td.border
        {
            border-bottom: 0.5px solid #909090;
        }
        span
        {
            font-size:10px;
        }
        span.fechas
        {
            font-size:8px;
        }
        .backg-blue
        {
            background-color: #0b4b8a;
            color:white;
        }
    </style>
    <br/>
    <br/>
    <br/>
    <table>
        <tr>
            <td width="120">
            </td>
            <td width="390" style="font-size:15px">
                UNIVERSIDAD NACIONAL AUT&Oacute;NOMA DE M&Eacute;XICO
            </td>
        </tr>
        <tr>
            <td width="120">
            </td>
            <td width="390" style="font-size:15px">
                FACULTAD DE ESTUDIOS SUPERIORES ACATL&Aacute;N
            </td>
        </tr>
    </table>
    <br/>
    <h4 align="right">FICHA DE DEP&Oacute;SITO TIENDA ACATL&Aacute;N</h4>
    <div class="space">    
        <div align="right">
        	<p class="subtitle">Esta ficha de deposito s&oacute;lo es v&aacute;lida para realizar el pago en M&eacute;xico</p>
        </div>
        <table>
            <tr>
            	<td width="160" height="40" class="border">
            		
            	</td>
            	<td width="350" style="line-height:45px;" class="border">
            		<span class="fechas"><b>Fecha de expedici&oacute;n: </b> </span><span>2018-08-01  </span>
            		<span class="fechas"><b>Fecha de Vigencia: </b></span><span>2018-09-01</span>
            	</td>
            </tr>
            <tr>
            	<td width="160" height="20" class="backg-blue border">
            		REFERENCIA
            	</td>
            	<td width="350" class="border">
            		44201 - 18000 - 202QY - 00271
            	</td>
            </tr>
         	<tr>
            	<td height="20" class="backg-blue border">
            		N&Uacute;MERO DE CONVENIO
            	</td>
            	<td class="border">
            		1407279
            	</td>
            </tr>
         	<tr>
            	<td height="20" class="backg-blue border">
            		CONCEPTO
            	</td>
            	<td class="border">
            		<span>Tienda en línea UNAM Pedido # 40</span>
            	</td>
            </tr>
         	<tr>
            	<td height="20" class="backg-blue border">
            		IMPORTE TOTAL
            	</td>
            	<td class="border">
            		$ 7466.54 
            	</td>
            </tr>
    	</table>
    	<div align="right">
    		<p style="font-size:7px; line-height:5px;">FACULTAD DE ESTUDIOS SUPERIORES ACATL&Aacute;N</p>
    		<p style="font-size:7px; line-height:5px;">Avenida Alcanfores y San Juan Totoltepe s/n, Sta Cruz Acatl&aacute;n, 53150 Naucalpan de Ju&aacute;rez, M&eacute;x. RFC: UNA2907227Y5</p>
    	</div>
        <h5 align="center" class="subtitle">La vigencia de esta ficha es de un mes a partir de la fecha de expedici&oacute;n<h5>
    </div>
EOF;
    $style = array(
            'align' => 'C',
            'cellfitalign' => 'C',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 6,
            'stretchtext' => 3
        );
    $barcode="44201 - 18000 - 202QY - 00271";
    $barcode=str_replace(" ", "", $barcode);
    $barcode=str_replace("-", "", $barcode);
    $obj_pdf->setBarcode($barcode);
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->write1DBarcode($barcode, 'C128', '', '', '', 16, 0.2, $style, 'N');
 	$obj_pdf->Output("40.pdf", "I");
?>